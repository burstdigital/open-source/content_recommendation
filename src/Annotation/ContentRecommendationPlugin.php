<?php

namespace Drupal\content_recommendation\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Headless Ninja Entity Manager Plugin item annotation object.
 *
 * @see \Drupal\hn\Plugin\HnEntityManagerPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class ContentRecommendationPlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
