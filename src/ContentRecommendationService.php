<?php

namespace Drupal\content_recommendation;

use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class HnResponseService.
 */
class ContentRecommendationService {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * Constructs a new HnResponseService object.
   *
   * @param ConfigFactory $config_factory
   * @param EventDispatcherInterface $eventDispatcher
   */
  public function __construct(ConfigFactory $config_factory, EventDispatcherInterface $eventDispatcher) {
    $this->config = $config_factory;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * @var array|null
   */
  private $entitiesByHandler = NULL;

  /**
   * Gathers all recommended content for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to get recommended content for.
   *
   * @param $query
   *
   * @return array
   */
  public function getContentRecommendation(EntityInterface $entity, $query) {

    /** @var \Drupal\content_recommendation\Plugin\ContentRecommendationPlugin $contentRecommendationPlugin */
    $contentRecommendationPlugin = \Drupal::getContainer()->get('plugin.manager.content_recommendation_plugin');

    /** @var \Drupal\content_recommendation\Plugin\ContentRecommendationPluginInterface[] $entityHandlers */
    $entityHandlers = $contentRecommendationPlugin->getEntityHandlers($entity);

    foreach ($entityHandlers as $entityHandler) {
      /** @var EntityInterface[] $entities_to_add */
      $entities_to_add = $entityHandler->recommend($entity, $query);

      $this->entitiesByHandler[$entityHandler->getPluginId()] = $entities_to_add;
    }

    return $this->entitiesByHandler ?: [];

  }

  /**
   * @param EntityInterface $entity
   * @return array
   */
  public function getContentRecommendationFlattened(EntityInterface $entity) {
    if ($this->entitiesByHandler === NULL) {
      $this->getContentRecommendationFlattened($entity);
    }

    return array_merge(...array_values($this->entitiesByHandler));
  }

}
