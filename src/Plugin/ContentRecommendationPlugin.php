<?php

namespace Drupal\content_recommendation\Plugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Content Recommendation plugin manager.
 */
class ContentRecommendationPlugin extends DefaultPluginManager {

  /**
   * Constructs a new ContentRecommendationPlugin object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ContentRecommendationPlugin', $namespaces, $module_handler, 'Drupal\content_recommendation\Plugin\ContentRecommendationPluginInterface', 'Drupal\content_recommendation\Annotation\ContentRecommendationPlugin');

    $this->alterInfo('content_recommendation_content_recommendation_plugin_info');
    $this->setCacheBackend($cache_backend, 'content_recommendation_content_recommendation_plugin_plugins');
  }

  /**
   * All plugin instances.
   *
   * @var \Drupal\hn\Plugin\HnEntityManagerPluginInterface[]
   */
  private $instances = [];

  /**
   * Gets the entity handlers for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that the handlers should be found for.
   *
   * @return \Drupal\content_recommendation\Plugin\ContentRecommendationPluginInterface[]
   *   Returns found handlers or empty array of non are found.
   */
  public function getEntityHandlers(EntityInterface $entity) {

    if (!$this->instances) {
      $definition_priorities = [];
      foreach ($this->getDefinitions() as $definition) {
        try {
          $this->instances[] = $this->createInstance($definition['id']);
        }
        catch (\Exception $exception) {
          // Skip.
          continue;
        }

        // Default priority = 0
        if (!isset($definition['priority'])) {
          $definition['priority'] = 0;
        }

        $definition_priorities[] = $definition['priority'];
      }
      // The plugin with the highest priority must be executed first.
      array_multisort($definition_priorities, SORT_DESC, $this->instances);
    }

    return array_filter($this->instances, function ($plugin) use ($entity) {
      /** @var ContentRecommendationPluginInterface $plugin */
      return $plugin->isSupported($entity);
    });

  }

}
