<?php

namespace Drupal\content_recommendation\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Base class for Headless Ninja Entity Manager Plugin plugins.
 */
abstract class ContentRecommendationPluginBase extends PluginBase implements ContentRecommendationPluginInterface {


  /**
   * The interface or class that this ContentRecommendation supports.
   *
   * @var string|\stdClass
   */
  protected $supports = '\Drupal\Core\Entity\EntityInterface';

  /**
   * {@inheritdoc}
   */
  public function isSupported(EntityInterface $entity) {
    return $entity instanceof $this->supports;
  }

}
