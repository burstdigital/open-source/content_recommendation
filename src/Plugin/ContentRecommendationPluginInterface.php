<?php

namespace Drupal\content_recommendation\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an interface for Content Recommendation Plugin plugins.
 */
interface ContentRecommendationPluginInterface extends PluginInspectionInterface {

  /**
   * Returns if the entity is supported by this Content Recommendation plugin.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return bool
   *   TRUE is the entity is supported, false if the entity isn't supported.
   */
  public function isSupported(EntityInterface $entity);

  /**
   * Handles the entity that is being viewed, and returns recommended entities.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that will be handled.
   *
   * @param $query
   *   The query params for the content recommendation.
   *
   * @return EntityInterface[]
   *   An array of Entities to be added as recommended content.
   */
  public function recommend(EntityInterface $entity, $query);

}
