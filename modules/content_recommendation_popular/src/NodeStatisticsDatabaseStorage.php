<?php

namespace Drupal\content_recommendation_popular;

use Drupal\Core\Database\Database;

class NodeStatisticsDatabaseStorage extends \Drupal\statistics\NodeStatisticsDatabaseStorage {

  public function fetchAllFiltered($limit, array $content_types, $order = 'totalcount') {
    // Build the query
    $select = Database::getConnection()
      ->select('node_counter', 'nc')
      ->fields('nc', ['nid']);

    $node_alias = $select->join('node', NULL, '%alias.nid = nc.nid');

    $node_data_alias = $select->join('node_field_data', NULL, "$node_alias.nid = %alias.nid");

    // Add conditions
    $select->condition("$node_data_alias.status", 1);
    $select->condition("$node_data_alias.type", $content_types, 'IN');

    $select->orderBy("nc.$order", 'DESC');
    $select->groupBy('nc.nid');

    $select->range(0, $limit);

    $nids = [];
    foreach ($select->execute()->fetchAll() as $nid) {
      $nids[] = $nid->nid;
    }

    return $nids;

  }

}
