<?php

namespace Drupal\content_recommendation_popular\EventSubscriber;

use Drupal\hn\Event\HnResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTotalViewsSubscriber.
 */
class EntityTotalViewsSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HnResponseEvent::POST_ENTITIES_ADDED => 'countNodeView',
    ];
  }

  public function countNodeView(HnResponseEvent $event) {
    /** @var \Drupal\node\Entity\Node $entity */
    $entity = $event->getEntryEntity();

    if ($entity->getEntityTypeId() !== 'node' || empty($entity->id())) {
      return;
    }

    /** @var \Drupal\statistics\StatisticsStorageInterface $statistics_service */
    $statistics_service = \Drupal::service('content_recommendation_popular.statistics.storage.node');

    $statistics_service->recordView($entity->id());
  }

}
