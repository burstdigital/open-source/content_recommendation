<?php

namespace Drupal\content_recommendation_popular\Plugin\ContentRecommendationPlugin;

use Drupal\content_recommendation\Plugin\ContentRecommendationPluginBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;

/**
 * Provides content recommendation for popular content
 *
 * @ContentRecommendationPlugin(
 *   id = "popular",
 *   priority = 10
 * )
 */
class ContentRecommendationPopular extends ContentRecommendationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function recommend(EntityInterface $entity, $query) {

    /** @var \Drupal\content_recommendation_popular\NodeStatisticsDatabaseStorage $statistics_service */
    $statistics_service = \Drupal::service('content_recommendation_popular.statistics.storage.node');

    // TODO: make configurable
    $content_types = ['area', 'news', 'page'];
    $limit_per_content_type = 1;

    $nids = [];

    foreach ($content_types as $content_type) {
      $result = $statistics_service->fetchAllFiltered($limit_per_content_type, [$content_type]);

      if (!empty($result)) {
        $nids[] = reset($result);
      }
    }

    $nodes = [];

    if (!empty($nids)) {
      $nodes = Node::loadMultiple($nids);
    }

    return $nodes;

  }

}
