<?php

namespace Drupal\content_recommendation_related;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\node\Entity\Node;

interface RelatedTypeInterface {

  /**
   * @param \Drupal\Core\Entity\Query\QueryInterface $entity_query
   * @param \Drupal\node\Entity\Node $entity
   * @param array $query
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   */
  public static function processQuery(QueryInterface $entity_query, Node $entity, array $query);

}
