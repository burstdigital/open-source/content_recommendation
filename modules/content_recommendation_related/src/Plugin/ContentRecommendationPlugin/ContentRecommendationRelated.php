<?php

namespace Drupal\content_recommendation_related\Plugin\ContentRecommendationPlugin;

use Drupal\content_recommendation\Plugin\ContentRecommendationPluginBase;
use Drupal\content_recommendation_hn\ContentRecommendationEntity;
use Drupal\content_recommendation_related\RelatedTypeClickstream1;
use Drupal\content_recommendation_related\RelatedTypeRandom;
use Drupal\content_recommendation_related\RelatedTypeTagBased;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;

/**
 * Provides content recommendation for related content
 *
 * @ContentRecommendationPlugin(
 *   id = "related",
 *   priority = 10
 * )
 */
class ContentRecommendationRelated extends ContentRecommendationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function recommend(EntityInterface $entity, $query) {

    /** @var Node $entity */

    if (!$entity->hasField('field_tags')) {
      return NULL;
    }

    $related_type = ContentRecommendationEntity::getRelatedTypeFromQuery($query);

    /* TODO: Make configurable */
    /** @var \Drupal\Core\Entity\Query\QueryInterface $entity_query */
    $entity_query = \Drupal::entityQuery('node')
      ->condition('type', ['area', 'route', 'page'], 'IN')
      ->condition('status', 1)
      ->condition('nid', $entity->id(), '!=')
      ->range(0, 3);

    switch ($related_type) {
      case ContentRecommendationEntity::RELATED_TYPES['random']:
        $nids = RelatedTypeRandom::processQuery($entity_query, $entity, $query);
        break;

      case ContentRecommendationEntity::RELATED_TYPES['clickstream_1']:
        $nids = RelatedTypeClickstream1::processQuery($entity_query, $entity, $query);

        if (count($nids) < 3) {
          $nids = RelatedTypeTagBased::processQuery($entity_query, $entity, $query);
        }
        break;

      default:
      case ContentRecommendationEntity::RELATED_TYPES['tag_based']:
        $nids = RelatedTypeTagBased::processQuery($entity_query, $entity, $query);
        break;
    }

    $nodes = [];

    if (!empty($nids)) {
      $nodes = Node::loadMultiple($nids);
    }

    return $nodes;

  }

}
