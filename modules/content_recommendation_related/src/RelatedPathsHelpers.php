<?php

namespace Drupal\content_recommendation_related;

abstract class RelatedPathsHelpers {

  const SCHEMA_NAME = 'content_recommendation_related_paths';

  const SCHEMA = [
    'description' => 'Stores example person entries for demonstration purposes.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: the autoincrement id',
      ],
      'requested_path' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'The path the related paths were requested for.',
      ],
      'related_path' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'The path that was matched as related for the requested_path.',
      ],
      'related_path_nid' => [
        'type' => 'int',
        'not null' => TRUE,
        'size' => 'small',
        'description' => 'The node id for the entity corresponding the related path.',
      ],
      'rating' => [
        'type' => 'float',
        'not null' => TRUE,
        'size' => 'small',
        'description' => 'The rating of the related_path. The higher the number, the better the match.',
      ],
      'avg_visit_time' => [
        'type' => 'float',
        'not null' => TRUE,
        'size' => 'small',
        'description' => 'The average visit duration that included both the requested_path and related_path.',
      ],
      'num_visits' => [
        'type' => 'int',
        'not null' => TRUE,
        'size' => 'small',
        'description' => 'The number of visits that included both the requested_path and related_path.',
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'size' => 'normal',
        'description' => 'Timestamp on which this record was created.',
      ],
      'expire' => [
        'type' => 'int',
        'not null' => TRUE,
        'size' => 'normal',
        'description' => 'Timestamp after which this record should expire.',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'requested_path' => ['requested_path'],
      'rating' => ['rating'],
      'expire' => ['expire'],
    ],
  ];

  /**
   * @param $path
   *   string Path to select related paths for.
   * @param int $expire
   *   Timestamp after which paths will not be valid anymore.
   *   Fallback is configurable through the configuration form. Pass -1 if all
   *   paths are required.
   *
   * @return array List of related paths for requested path.
   */
  public static function getRelatedPaths($path, $expire = NULL) {
    //        $schema = \Drupal\Core\Database\Database::getConnection()->schema();
    //        $schema->dropTable(self::SCHEMA_NAME);
    //        $schema = \Drupal\Core\Database\Database::getConnection()->schema();
    //        $schema->createTable(self::SCHEMA_NAME, self::SCHEMA);
    $expire = self::getExpire($expire);

    $database = \Drupal::database();

    // Get related paths for requested path.
    $related_paths_query = $database->select(self::SCHEMA_NAME, 'related_paths');

    // Add fields.
    $related_paths_query->addField('related_paths', 'related_path_nid');

    $related_paths_query->condition('requested_path', $path);

    if ($expire !== -1) {
      // Filter out all paths that are expired.
      $related_paths_query->condition('expire', time(), '>');
    }

    $result = $related_paths_query->execute();

    $related_paths = [];

    foreach ($result->fetchAll() as $related_path) {
      $related_paths[] = (array) $related_path;
    }

    return $related_paths;
  }

  /**
   * @param $path
   *   string Path to select related paths for.
   * @param $related_paths
   * @param bool $replace
   *   Set to FALSE if you want to keep old records for this $path.
   * @param int $expire
   *   Timestamp after which paths will not be valid anymore.
   *   Fallback is configurable through the configuration form. Pass -1 if all
   *   paths are required.
   *
   * @return void
   */
  public static function setRelatedPaths($path, $related_paths, $replace = TRUE, $expire = NULL) {

    $database = \Drupal::database();

    // Delete old records for requested path.
    if ($replace) {
      $database->delete(self::SCHEMA_NAME)
        ->condition('requested_path', $path)
        ->execute();
    }

    $expire = self::getExpire($expire);

    foreach ($related_paths as $related_path) {
      $fields = [
        'requested_path' => $path,
        'related_path' => $related_path['related_path'],
        'related_path_nid' => $related_path['related_path_nid'],
        'avg_visit_time' => $related_path['avg_visit_time'],
        'num_visits' => $related_path['num_visits'],
        'rating' => $related_path['rating'],
        'created' => time(),
        'expire' => $expire,
      ];

      try {
        $database->insert(self::SCHEMA_NAME)
          ->fields($fields)
          ->execute();
      }
      catch (\Exception $e) {
      }
    }

  }

  public static function deleteExpiredRelatedPaths() {

    $database = \Drupal::database();

    $database->delete(self::SCHEMA_NAME)
      ->condition('expire', time(), '<')
      ->execute();

  }

  /**
   * @param int $expire
   *   Timestamp after which paths will not be valid anymore.
   *   Fallback is configurable through the configuration form. Pass -1 if all
   *   paths are required.
   *
   * @return int Provided or default expire timestamp.
   */
  public static function getExpire($expire = NULL) {
    if (is_null($expire)) {
      // TODO: make configurable
      $expire = time() + 900;
    }

    return $expire;
  }

  /**
   * Get base domain prepended to all paths in Matomo. Configurable through config form.
   *
   * @return string
   */
  public static function getMatomoBasePath() {
    return self::getConfig('matomo_base_path');
  }

  public static function getConfig($key) {
    return \Drupal::config('content_recommendation_related.settings')->get($key);
  }

}
