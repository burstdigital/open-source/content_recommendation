<?php

namespace Drupal\content_recommendation_related;

use Drupal\Core\Database\Query\Select;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\node\Entity\Node;

abstract class RelatedTypeClickstream1 implements RelatedTypeInterface {

  /**
   * @inheritdoc
   */
  public static function processQuery(QueryInterface $entity_query, Node $entity, array $query) {

    $path = explode('?', $query['path'])[0];

    $related_paths = RelatedPathsHelpers::getRelatedPaths($path);

    //  If path wasn't in the cache yet, fetch new ones from Matomo.
    if (empty($related_paths) || $query['f'] === 'true') {
      try {
        $matomo_related_paths = self::getRelatedPathsFromMatomo($path);

        if (!empty($matomo_related_paths)) {
          $related_paths = self::processRelatedPathsFromMatomo($path, $matomo_related_paths);

          RelatedPathsHelpers::setRelatedPaths($path, $related_paths);
        }
      }
      catch (\Exception $exception) {
      }
    }

    // Return only nids of related paths.
    return array_column($related_paths, 'related_path_nid');
  }

  public static function getMockRelatedPathsFromMatomo($path) {
    return [
    //      (object) [
    //        'path_in_clickstream' => 'natuurmonumenten.nl/',
    //        'avg_visit_time_for_this_path' => '496.0000',
    //        'num_visits_that_include_this_path' => '1',
    //      ],
      (object) [
        'path_in_clickstream' => 'natuurmonumenten.nl/bezoekerscentrum-gooi-en-vechtstreek/agenda/oerrr-ontmoet-de-huisimker-en-haar-bijen',
        'avg_visit_time_for_this_path' => '496.0000',
        'num_visits_that_include_this_path' => '1',
      ],
      (object) [
        'path_in_clickstream' => 'natuurmonumenten.nl/kinderen/help-de-bij/alles-over-de-bij',
        'avg_visit_time_for_this_path' => '496.0000',
        'num_visits_that_include_this_path' => '1',
      ],
      (object) [
        'path_in_clickstream' => 'natuurmonumenten.nl/kinderen/helpdebij',
        'avg_visit_time_for_this_path' => '496.0000',
        'num_visits_that_include_this_path' => '1',
      ],
      (object) [
        'path_in_clickstream' => 'natuurmonumenten.nl/nieuws/reekalf-doodgebeten-door-loslopende-hond',
        'avg_visit_time_for_this_path' => '496.0000',
        'num_visits_that_include_this_path' => '1',
      ],
      //      (object) [
      //        'path_in_clickstream' => 'natuurmonumenten.nl/oerrr/aanvragen/actiepakket',
      //        'avg_visit_time_for_this_path' => '496.0000',
      //        'num_visits_that_include_this_path' => '1',
      //      ],
      (object) [
        'path_in_clickstream' => 'natuurmonumenten.nl/standpunten/schaapskuddes',
        'avg_visit_time_for_this_path' => '496.0000',
        'num_visits_that_include_this_path' => '1',
      ],
      //      (object) [
      //        'path_in_clickstream' => 'natuurmonumenten.nl/nieuws',
      //        'avg_visit_time_for_this_path' => '437.5000',
      //        'num_visits_that_include_this_path' => '2',
      //      ],
      (object) [
        'path_in_clickstream' => 'natuurmonumenten.nl/nieuws/natuurmonumenten-en-natuurbegraven-nederland-tekenen-overeenkomst',
        'avg_visit_time_for_this_path' => '437.5000',
        'num_visits_that_include_this_path' => '2',
      ],
      (object) [
        'path_in_clickstream' => 'natuurmonumenten.nl/nieuws/bloembollen-voor-nieuwe-leden-op-tuinidee',
        'avg_visit_time_for_this_path' => '379.0000',
        'num_visits_that_include_this_path' => '1',
      ],
      (object) [
        'path_in_clickstream' => 'natuurmonumenten.nl/nieuws/natuurmonumenten-ontvangt-ruim-14-miljoen-van-nationale-postcode-loterij',
        'avg_visit_time_for_this_path' => '379.0000',
        'num_visits_that_include_this_path' => '1',
      ],
    ];
  }

  /**
   * @param $path
   *   string Path of which to get related items
   *
   * @return array List from Matomo of related paths based on requested path.
   * @throws \Exception
   */
  public static function getRelatedPathsFromMatomo($path) {
//    return self::getMockRelatedPathsFromMatomo($path);

    $lock = \Drupal::lock();

    // Activate lock to prevent parallel queries to Matomo (as per ToS).
    if (!$lock->acquire('content_recommendation_related_matomo_query')) {
      // If lock couldn't be acquired, wait. Code will continue if lock is acquired again before timeout.
      $lock->wait('content_recommendation_related_matomo_query');
      if (!$lock->acquire('content_recommendation_related_matomo_query')) {
        throw new \Exception('Unable to acquire lock.');
      }
    }

    // Get Matomo database connection.
    $connection = MatomoDatabaseConnection::getConnection();

    if (!$connection) {
      throw new \Exception('Couldn\'t establish connection to Matomo database');
    }

    // Get domain that is prefixed to each path in Matomo.
    $matomo_base_path = RelatedPathsHelpers::getMatomoBasePath();

    $min_visit_duration = RelatedPathsHelpers::getConfig('min_visit_duration');
    $max_visit_duration = RelatedPathsHelpers::getConfig('max_visit_duration');

    /**
     * Query steps
     *
     * Get clickstreams
     * 1.1   Get all visits with a duration between 1 and 30 minutes.
     * 1.2   Filter out all visits that don't include the requested path.
     * 1.3   Get all clickstreams of leftover visits.
     * 1.4   Deduplicate all paths within a click stream.
     *
     * Calculate totals and averages.
     * 2.1   Get average visit time per path. If a path is present in 3 visits,
     * the average duration of those 3 visits is added as column to the path.
     * 2.2   Get number of times this path is present in a clickstream.
     * 2.2   Group all duplicate paths into one row with calculated average and count.
     *
     * Filter
     * 3.1   Filter out paths that were visited less than 2 times in combination with the requested path.
     * 3.2   Order by average visit duration descending.
    */

    $clickstreams_query = new Select('log_visit', 'visit', $connection);

    $clickstreams_query->addField('visit', 'idvisit', 'visit_id');
    $clickstreams_query->addField('visit', 'visit_total_time', 'visit_time');

    // Filter out visits of which the duration was not between 1 and 30 minutes.
    $clickstreams_query->condition('visit.visit_total_time', $min_visit_duration, '>=');
    $clickstreams_query->condition('visit.visit_total_time', $max_visit_duration, '<=');

    // Join between visits and junction table of visits and actions. This junction table contains all actions in a visit.
    $visit_action_alias = $clickstreams_query->join('log_link_visit_action', 'visit_action', 'visit.idvisit = %alias.idvisit');

    // Join between actions and junction table of visits and actions. This action table contains all actions that can possibly be triggered during a visit.
    $action_alias = $clickstreams_query->join('log_action', 'action', "$visit_action_alias.idaction_url = %alias.idaction");

    $clickstreams_query->condition("$action_alias.type", 1);
    $clickstreams_query->where("SUBSTRING_INDEX($action_alias.name, '?', 1) = '" . $matomo_base_path . $path . "'");

    // Join again between leftover visits and junction table of visits and actions. This will get all page views in the clickstream during a visit.
    $visit_action_2_alias = $clickstreams_query->join('log_link_visit_action', 'visit_action_2', 'visit.idvisit = %alias.idvisit');
    $action_2_alias = $clickstreams_query->join('log_action', 'action_2', "$visit_action_2_alias.idaction_url = %alias.idaction");

    // Select the path in the clickstream, without possible query string.
    $clickstreams_query->addExpression("SUBSTRING_INDEX($action_2_alias.name, '?', 1)", 'path_in_clickstream');

    // Filter out everything that is not an internal page view.
    $clickstreams_query->condition("$action_2_alias.type", 1);
    // Filter out the requested path.
    $clickstreams_query->where("SUBSTRING_INDEX($action_2_alias.name, '?', 1) != '" . $matomo_base_path . $path . "'");
    // Only support certain content bundles.
    // TODO: make configurable.
//    $clickstreams_query->condition("$visit_action_2_alias.custom_dimension_1", ['area', 'page', 'news', 'route', 'activity'], 'IN');

    // Group all paths in the same visit.
    $clickstreams_query->groupBy('visit_id');
    $clickstreams_query->groupBy('path_in_clickstream');

    $calculate_totals_and_averages_query = new Select($clickstreams_query, 'clickstreams', $connection);

    $calculate_totals_and_averages_query->addField('clickstreams', 'path_in_clickstream', 'path_in_clickstream');
    $calculate_totals_and_averages_query->addExpression('AVG(clickstreams.visit_time)', 'avg_visit_time_for_this_path');
    $calculate_totals_and_averages_query->addExpression('count(clickstreams.visit_id)', 'num_visits_that_include_this_path');

    // Group all paths to get unique list of paths.
    $calculate_totals_and_averages_query->groupBy('clickstreams.path_in_clickstream');

    // Filter results.
    $filter_query = new Select($calculate_totals_and_averages_query, 'related_paths', $connection);

    // Proxy fields from previous query.
    $filter_query->addField('related_paths', 'path_in_clickstream');
    $filter_query->addField('related_paths', 'avg_visit_time_for_this_path');
    $filter_query->addField('related_paths', 'num_visits_that_include_this_path');

    // Filter out paths that were visited less than 2 times in combination with the requested path.
//    $filter_query->condition('related_paths.num_visits_that_include_this_path', 2, '>');

    // Filter out paths that were visited less than 2 times in combination with the requested path.
    $filter_query->orderBy('related_paths.avg_visit_time_for_this_path', 'DESC');

    $result = $filter_query->execute();

    // Release lock so the next query can proceed.
    $lock->release('content_recommendation_related_matomo_query');

    return $result->fetchAll();
  }

  /**
   * @param $path
   *   string Path of which to get related items
   * @param $matomo_related_paths object[]
   *
   * @return array Processed related paths ready for inserting into database.
   */
  public static function processRelatedPathsFromMatomo($path, $matomo_related_paths) {
    $related_paths = [];

    $matomo_base_path = RelatedPathsHelpers::getMatomoBasePath();

    $num_visits_for_path_weight = RelatedPathsHelpers::getConfig('num_visits_for_path_weight');

    /**
     * Bayesian Statistics
     * bc = ((avg_num_votes * avg_rating) + (this_num_votes * this_rating)) / (avg_num_votes + this_num_votes)
     */

    // Total number of visits in this list of paths. A visit is counted for each path, so 10 visits with all 10 paths will result in a total of 10 * 10 = 100.
    $total_num_visits = 0;
    // Total visit time in this list of paths. A visit time is counted for each path, so 10 visits of 10 seconds with all 10 paths will result in a total of 10 * 10 * 10 = 1000.
    $total_visit_time = 0;

    // Get total number of visits and total visit time.
    foreach ($matomo_related_paths as $matomo_related_path) {
      $total_num_visits += (int) $matomo_related_path->num_visits_that_include_this_path;
      $total_visit_time += (float) $matomo_related_path->avg_visit_time_for_this_path;
    }

    // Average number of visits per path.
    $avg_num_visits = $total_num_visits / count($matomo_related_paths);
    // Average visit time per path.
    $avg_visit_time = $total_visit_time / count($matomo_related_paths);

    $bayesian_base = $avg_num_visits * $avg_visit_time;

    // Prepare every related path for insertion.
    foreach ($matomo_related_paths as $matomo_related_path) {
      $related_path = str_replace($matomo_base_path, '', $matomo_related_path->path_in_clickstream);
      $avg_visit_time_for_path = (float) $matomo_related_path->avg_visit_time_for_this_path;
      $num_visits_for_path = (int) $matomo_related_path->num_visits_that_include_this_path;

      // Bayesian statistics to weigh average visit time for path and number of visits for path.
      $bayes_rating = ($bayesian_base + ($avg_visit_time_for_path * pow($num_visits_for_path, $num_visits_for_path_weight))) / ($avg_num_visits + $num_visits_for_path);

      $related_paths[] = [
        'requested_path' => $path,
        'related_path' => $related_path,
        'avg_visit_time' => $avg_visit_time_for_path,
        'num_visits' => $num_visits_for_path,
        'rating' => $bayes_rating,
      ];
    }

    // Sort related_paths based on calculated rating.
    uasort($related_paths, 'self::sortRelatedPaths');

    // Get top 3 highest ranked related paths.
    $top_related_paths = [];

    /** @var \Drupal\hn\Plugin\HnPathResolverManager $path_resolver */
    $path_resolver = \Drupal::service('hn.path_resolver');

    // Get node id's for top related paths.
    foreach ($related_paths as $related_path) {
      // Stop after 3 related paths were found.
      if (count($top_related_paths) === 3) {
        break;
      }

      /** @var \Drupal\hn\HnPathResolverResponse $entity_response */
      $entity_response = $path_resolver->resolve($related_path['related_path']);

      /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
      $entity = $entity_response->getEntity();

      if (!empty($entity)) {
        // TODO: Make configurable.
        if (
          $entity->bundle() === 'area' ||
          $entity->bundle() === 'route' ||
          $entity->bundle() === 'page' ||
          $entity->bundle() === 'news' ||
          (
            $entity->bundle() === 'activity' &&
            $entity->get('field_activity_shown')->getValue()[0]['value'] === '1' &&
            $entity->get('field_bookable')->getValue()[0]['value'] === '1'
          )
        ) {
          // Extend related path with found node id and push it as top related path.
          $top_related_paths[] = $related_path + ['related_path_nid' => (int) $entity->id()];
        }
      }
    }

    return $top_related_paths;
  }

  public static function sortRelatedPaths($a, $b) {
    if ($a['rating'] === $b['rating']) {
      return 0;
    }

    return ($a['rating'] > $b['rating']) ? -1 : 1;
  }

}
