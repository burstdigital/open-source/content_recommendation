<?php

namespace Drupal\content_recommendation_related;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\node\Entity\Node;

abstract class RelatedTypeRandom implements RelatedTypeInterface {

  /**
   * @inheritdoc
   */
  public static function processQuery(QueryInterface $entity_query, Node $entity, array $query) {
    $entity_query->addTag('order_random');

    return $entity_query->execute();
  }

}
