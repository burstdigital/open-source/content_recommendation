<?php

namespace Drupal\content_recommendation_related;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\node\Entity\Node;

abstract class RelatedTypeTagBased implements RelatedTypeInterface {

  /**
   * @inheritdoc
   */
  public static function processQuery(QueryInterface $entity_query, Node $entity, array $query) {
    if ($tags = $entity->get('field_tags')->getValue()) {
      $tag_ids = [];

      foreach ($tags as $tag) {
        $tag_ids[] = $tag['target_id'];
      }

      $entity_query
        ->addMetaData('tags', $tag_ids)
        ->addMetaData('entity_id', $entity->id())
        ->addTag('order_by_tags_relevance');
    }

    return $entity_query->execute();
  }

}
