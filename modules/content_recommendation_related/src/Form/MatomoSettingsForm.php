<?php

namespace Drupal\content_recommendation_related\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 */
class MatomoSettingsForm extends ConfigFormBase {
  const STATE_KEY = 'content_recommendation_related.matomo_settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_recommendation_hn_matomo_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $state = \Drupal::state();

    $matomo_credentials = $state->get(self::STATE_KEY);

    $form['matomo_credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Matomo credentials'),
    ];

    $form['matomo_credentials']['db_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database hostname'),
      '#description' => $this->t('Please provide the hostname or IP address your Matomo database is hosted at.'),
      '#required' => TRUE,
      '#default_value' => $matomo_credentials['db_host'] ?: '',
    ];

    $form['matomo_credentials']['db_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database username'),
      '#description' => $this->t('Please provide the username to log into your Matomo database.'),
      '#required' => TRUE,
      '#default_value' => $matomo_credentials['db_username'] ?: '',
    ];

    $form['matomo_credentials']['db_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database password'),
      '#description' => $this->t('Please provide the password to log into your Matomo database. Leave empty if no password is required.'),
      '#default_value' => $matomo_credentials['db_password'] ?: '',
    ];

    $form['matomo_credentials']['db_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database name'),
      '#description' => $this->t('Please provide the database name to connect with.'),
      '#required' => TRUE,
      '#default_value' => $matomo_credentials['db_name'] ?: '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $state = \Drupal::state();

    $state->set(self::STATE_KEY, [
      'db_host' => $values['db_host'],
      'db_username' => $values['db_username'],
      'db_password' => $values['db_password'],
      'db_name' => $values['db_name'],
    ]);

    parent::submitForm($form, $form_state);
  }

}
