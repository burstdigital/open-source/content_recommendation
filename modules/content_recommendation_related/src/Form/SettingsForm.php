<?php

namespace Drupal\content_recommendation_related\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 */
class SettingsForm extends ConfigFormBase {

  const CONFIG_KEY = 'content_recommendation_related.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::CONFIG_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_recommendation_related_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_KEY);

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General settings'),
    ];

    $form['general']['min_visit_duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum visit duration (seconds)'),
      '#description' => $this->t('Enter the number in seconds of how long a visit at least had to be before being taken into account for related paths.'),
      '#default_value' => $config->get('min_visit_duration'),
    ];

    $form['general']['max_visit_duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum visit duration (seconds)'),
      '#description' => $this->t('Enter the number in seconds of how long a visit at most may have been before being taken into account for related paths.'),
      '#default_value' => $config->get('max_visit_duration'),
    ];

    $form['general']['expire_related_paths'] = [
      '#type' => 'number',
      '#title' => $this->t('Expire related paths (seconds)'),
      '#description' => $this->t('Enter the number in seconds of how long results from Matomo for a certain path should remain valid.'),
      '#default_value' => $config->get('expire_related_paths'),
    ];

    $form['matomo_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Matomo settings'),
    ];

    $form['matomo_settings']['matomo_base_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Matomo base path'),
      '#description' => $this->t('Enter the base path that is prepended to all paths in Matomo. This usually is your domain without protocol and subdomain (e.g. https://www.example.com become example.com).'),
      '#default_value' => $config->get('matomo_base_path'),
    ];

    $form['matomo_settings']['num_visits_for_path_weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight of the number of visits per path'),
      '#description' => $this->t('Enter the weight you give to the number of path combinations between the requested and related paths. This will be used to determine which path combination is ranked higher than the other. A value between 1 and 2 would be common.'),
      '#default_value' => $config->get('num_visits_for_path_weight'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the config.
    $this->config(self::CONFIG_KEY)
      ->set('min_visit_duration', $values['min_visit_duration'])
      ->set('max_visit_duration', $values['max_visit_duration'])
      ->set('expire_related_paths', $values['expire_related_paths'])
      ->set('matomo_base_path', $values['matomo_base_path'])
      ->set('num_visits_for_path_weight', $values['num_visits_for_path_weight'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
