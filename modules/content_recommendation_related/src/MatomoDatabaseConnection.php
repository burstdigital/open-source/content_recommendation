<?php

namespace Drupal\content_recommendation_related;

use Drupal\content_recommendation_related\Form\MatomoSettingsForm;
use Drupal\Core\Database\Driver\mysql\Connection;

abstract class MatomoDatabaseConnection {

  /**
   *
   */
  public static function getConnection() {
    $state = \Drupal::state();
    $matomo_credentials = $state->get(MatomoSettingsForm::STATE_KEY);

    $dsn = 'mysql:dbname=' . $matomo_credentials['db_name'] . ';host=' . $matomo_credentials['db_host'];

    try {
      $pdo = new \PDO($dsn, $matomo_credentials['db_username'], $matomo_credentials['db_password']);

      return new Connection($pdo);
    }
    catch (\Exception $exception) {

    }

    return NULL;
  }

}
