<?php

namespace Drupal\content_recommendation_hn\Plugin\HnEntityManagerPlugin;

use Drupal\content_recommendation_hn\ContentRecommendationEntity;
use Drupal\content_recommendation_hn\EventSubscriber\ComputeContentRecommendationSubscriber;
use Drupal\Core\Entity\EntityInterface;
use Drupal\hn\Plugin\HnEntityManagerPluginBase;

/**
 * Provides a Content Recommendation Item Handler
 *
 * @HnEntityManagerPlugin(
 *   id = "content_recommendation_items",
 *   priority = 10
 * )
 */
class ContentRecommendationItemsHandler extends HnEntityManagerPluginBase {

  /**
   * Only the MapAPIDetailsEntity is supported
   */
  protected $supports = ContentRecommendationEntity::class;

  /**
   * {@inheritdoc}
   */
  public function handle(EntityInterface $entity, $view_mode = 'default') {
    /** @var ContentRecommendationEntity $entity */
    $entity_path = $entity->getPath();
    $query = $entity->getQuery();

    /** @var \Drupal\hn\Plugin\HnPathResolverManager $path_resolver */
    $path_resolver = \Drupal::service('hn.path_resolver');

    /** @var \Drupal\hn\HnPathResolverResponse $entity_response */
    $entity_response = $path_resolver->resolve($entity_path);

    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $entity_response->getEntity();

    return ComputeContentRecommendationSubscriber::computeContentRecommendation($entity, $query, $view_mode);

  }

}
