<?php

namespace Drupal\content_recommendation_hn\Plugin\HnPathResolver;

use Drupal\content_recommendation_hn\ContentRecommendationEntity;
use Drupal\hn\HnPathResolverResponse;
use Drupal\hn\Plugin\HnPathResolverBase;

/**
 * This provides a Content Recommendation Items Resolver
 *
 * @HnPathResolver(
 *   id = "content_recommendation_items",
 *   priority = 100
 * )
 */
class ContentRecommendationItemsResolver extends HnPathResolverBase {

  /**
   * {@inheritdoc}
   */
  public function resolve($path) {
    $path = explode('?', ltrim($path, '/'));
    if ($path[0] !== 'api/content-recommendation') {
      return NULL;
    }

    parse_str($path[1], $query);
    if (empty($query['path'])) {
      return NULL;
    }

    return new HnPathResolverResponse(new ContentRecommendationEntity($query['path'], $query));
  }

}
