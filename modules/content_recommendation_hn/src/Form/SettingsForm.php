<?php

namespace Drupal\content_recommendation_hn\Form;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_recommendation_hn.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_recommendation_hn_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('content_recommendation_hn.settings');

    $entity_types = \Drupal::entityTypeManager()->getDefinitions();
    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service */
    $bundle_info_service = \Drupal::service('entity_type.bundle.info');

    $supported_bundles = $config->get('supported_bundles');

    $form['supported_bundles'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Supported entities'),
    ];

    foreach ($entity_types as $entity_type_id => $entity_type) {
      $supported_entity_index = array_search($entity_type_id, array_column($supported_bundles, 'entity_type_id'));
      $supported_entity = $supported_entity_index !== FALSE ? $supported_bundles[$supported_entity_index] : FALSE;

      $bundles = $bundle_info_service->getBundleInfo($entity_type_id);

      if (empty($bundles)) {
        continue;
      }

      $form['supported_bundles']['entity_' . $entity_type_id] = [
        '#type' => 'details',
        '#open' => FALSE,
        '#title' => $entity_type->getLabel() . ' (' . $entity_type_id . ')',
      ];

      foreach ($bundles as $bundle_name => $bundle_info) {
        $supported_bundle_index = !empty($supported_entity['bundles']) ? array_search($bundle_name, array_column($supported_entity['bundles'], 'bundle')) : FALSE;
        $supported_bundle = $supported_bundle_index !== FALSE ? $supported_entity['bundles'][$supported_bundle_index] : FALSE;

        $form['supported_bundles']['entity_' . $entity_type_id]['bundle_' . $entity_type_id . '_' . $bundle_name] = [
          '#type' => 'checkbox',
          '#title' => $bundle_info['label'] . ' (' . $bundle_name . ')',
          '#default_value' => !!$supported_bundle,
        ];

        if ($entity_type->entityClassImplements(FieldableEntityInterface::class)) {

          $options = [];
          /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager */
          $entity_field_manager = \Drupal::service('entity_field.manager');
          $fields = $entity_field_manager->getFieldDefinitions($entity_type_id, $bundle_name);
          foreach ($fields as $field_id => $field) {
            $field_title = $field_id;
            if ($field->getLabel()) {
              $field_title .= ' (' . $field->getLabel() . ')';
            }
            $options[$field_id] = $field_title;
          }

          $bundle_checked_state = [':input[name="bundle_' . $entity_type_id . '_' . $bundle_name . '"]' => ['checked' => FALSE]];

          /* TODO: multiple conditions */
          $form['supported_bundles']['entity_' . $entity_type_id]['field_name_' . $entity_type_id . '_' . $bundle_name] = [
            '#type' => 'select',
            '#title' => $this->t('Field name'),
            '#options' => $options,
            "#empty_option" => $this->t('- Select -'),
            '#default_value' => $supported_bundle['field_name'] ?: '',
            '#description' => $this->t('Additionally select a field and enter a value. Only when the entity has this value in this field, it will be supported. This could allow for having a checkbox on an entity to show/hide content recommendation. Leave empty if all <em>:bundle_name</em> entities are supported.', [':bundle_name' => $bundle_name]),
            '#states' => [
              'invisible' => $bundle_checked_state,
            ],
          ];

          $field_name_state = ['select[name="field_name_' . $entity_type_id . '_' . $bundle_name . '"]' => ['value' => '']];

          $form['supported_bundles']['entity_' . $entity_type_id]['field_operator_' . $entity_type_id . '_' . $bundle_name] = [
            '#type' => 'select',
            '#options' => [
              'IS' => $this->t('IS'),
              'IS_NOT' => $this->t('IS NOT'),
              'EMPTY' => $this->t('EMPTY'),
              'NOT_EMPTY' => $this->t('NOT EMPTY'),
            ],
            '#title' => $this->t('Field operator'),
            '#default_value' => $supported_bundle['field_operator'] ?: 'IS',
            '#states' => [
              'invisible' => [
                [$bundle_checked_state],
                [$field_name_state],
              ],
            ],
          ];

          $field_operator_state_EMPTY = [
            'select[name="field_operator_' . $entity_type_id . '_' . $bundle_name . '"]' => ['value' => 'EMPTY'],
          ];

          $field_operator_state_NOT_EMPTY = [
            'select[name="field_operator_' . $entity_type_id . '_' . $bundle_name . '"]' => ['value' => 'NOT_EMPTY'],
          ];

          $form['supported_bundles']['entity_' . $entity_type_id]['field_value_' . $entity_type_id . '_' . $bundle_name] = [
            '#type' => 'textfield',
            '#title' => $this->t('Field value'),
            '#default_value' => isset($supported_bundle['field_value']) ? $supported_bundle['field_value'] : '',
            '#states' => [
              'invisible' => [
                [$field_name_state],
                [$field_operator_state_EMPTY],
                [$field_operator_state_NOT_EMPTY],
              ],
            ],
          ];
        }
      }

    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $entities = [];

    $entity_types = \Drupal::entityTypeManager()->getDefinitions();
    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service */
    $bundle_info_service = \Drupal::service('entity_type.bundle.info');

    foreach ($entity_types as $entity_type_id => $entity_type) {
      $entity = [
        'entity_type_id' => $entity_type_id,
        'bundles' => [],
      ];

      $bundles = $bundle_info_service->getBundleInfo($entity_type_id);

      if (empty($bundles)) {
        continue;
      }

      foreach ($bundles as $bundle_name => $bundle_info) {
        if (empty($values['bundle_' . $entity_type_id . '_' . $bundle_name])) {
          continue;
        }

        $bundle = [
          'bundle' => $bundle_name,
        ];

        if ($entity_type->entityClassImplements(FieldableEntityInterface::class)) {
          $bundle['field_name'] = $values['field_name_' . $entity_type_id . '_' . $bundle_name];
          $bundle['field_value'] = $values['field_value_' . $entity_type_id . '_' . $bundle_name];
          $bundle['field_operator'] = $values['field_operator_' . $entity_type_id . '_' . $bundle_name];
        }

        $entity['bundles'][] = $bundle;
      }

      if (!empty($entity['bundles'])) {
        $entities[] = $entity;
      }

    }

    // Save the config.
    $this->config('content_recommendation_hn.settings')
      ->set('supported_bundles', $entities)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
