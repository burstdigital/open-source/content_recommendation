<?php

namespace Drupal\content_recommendation_hn;

use Drupal\Core\Entity\Entity;

class ContentRecommendationEntity extends Entity {

  private $path;

  private $query;

  const RELATED_TYPES = [
    'default' => 'default',
    'tag_based' => 'tag_based',
    'random' => 'random',
    'clickstream_1' => 'clickstream_1',
  ];

  public function __construct($path, $query) {
    parent::__construct([], '');
    $this->path = $path;
    $this->query = $query;
  }

  public function getPath() {
    return $this->path;
  }

  public function getQuery() {
    return $this->query;
  }

  public function getRelatedType() {
    return static::getRelatedTypeFromQuery($this->getQuery());
  }

  public function uuid() {
    return 'content_recommendation_items_' . $this->path;
  }

  public static function getRelatedTypeFromQuery($query) {
    if (!empty($query['related_type']) && !empty($related_type = self::RELATED_TYPES[$query['related_type']])) {
      return $related_type;
    }

    return self::RELATED_TYPES['default'];
  }

}
