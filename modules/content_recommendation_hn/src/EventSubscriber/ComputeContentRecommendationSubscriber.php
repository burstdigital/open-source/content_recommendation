<?php

namespace Drupal\content_recommendation_hn\EventSubscriber;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\hn\Event\HnHandledEntityEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTotalViewsSubscriber.
 */
class ComputeContentRecommendationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HnHandledEntityEvent::POST_HANDLE => 'onPostHandle',
    ];
  }

  /**
   * @param EntityInterface $entity
   * @return bool
   */
  public static function entitySupportsContentRecommendation(EntityInterface $entity, $view_mode) {
    // TODO: make configurable per bundle
    if ($view_mode !== 'default') {
      return FALSE;
    }

    $config = \Drupal::config('content_recommendation_hn.settings');
    $supported_bundles = $config->get('supported_bundles');

    if (empty($config) || empty($supported_bundles)) {
      return FALSE;
    }

    $supported_entity_index = array_search($entity->getEntityTypeId(), array_column($supported_bundles, 'entity_type_id'));
    $supported_entity = $supported_entity_index !== FALSE ? $supported_bundles[$supported_entity_index] : FALSE;

    // If this entity type has no supported bundles, quit.
    if ($supported_entity === FALSE) {
      return FALSE;
    }

    $supported_bundle_index = !empty($supported_entity['bundles']) ? array_search($entity->bundle(), array_column($supported_entity['bundles'], 'bundle')) : FALSE;
    $supported_bundle = $supported_bundle_index !== FALSE ? $supported_entity['bundles'][$supported_bundle_index] : FALSE;

    // If this specific bundle isn't supported, quit.
    if ($supported_bundle === FALSE) {
      return FALSE;
    }

    // If no field conditions are set or entity is not fieldable, then this bundle is supported.
    if (empty($supported_bundle['field_name']) || !$entity instanceof FieldableEntityInterface) {
      return TRUE;
    }

    /** @var FieldableEntityInterface $entity */

    // If entity doesn't have field, quit.
    if (!$entity->hasField($supported_bundle['field_name'])) {
      return FALSE;
    }

    $entity_field_value = $entity->get($supported_bundle['field_name'])->getValue();

    // If entity value doesn't equal configured value, quit.
    switch ($supported_bundle['field_operator']) {
      case 'EMPTY':
        // If operator is set to EMPTY, value should be empty, quit otherwise.
        return empty($entity_field_value) || empty($entity_field_value[0]['value']);

      case 'NOT_EMPTY':
        // If operator is set to NOT_EMPTY, value should not be empty, quit otherwise.
        return !empty($entity_field_value[0]['value']);

      case 'IS':
        // If operator is set to IS, then values should be equal, quit otherwise.
        return (empty($entity_field_value[0]['value']) && empty($supported_bundle['field_value'])) || $entity_field_value[0]['value'] === $supported_bundle['field_value'];

      case 'IS_NOT':
        // If operator is set to IS_NOT, then values should not be equal, quit otherwise.
        if (empty($entity_field_value[0]['value'])) {
          return $supported_bundle['field_value'] !== '';
        }
        return $entity_field_value[0]['value'] !== $supported_bundle['field_value'];
    }

    // Else, quit.
    return FALSE;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $query
   * @param string $view_mode
   *
   * @return array
   */
  public static function computeContentRecommendation(EntityInterface $entity, $query, $view_mode = 'default') {
    /** @var \Drupal\content_recommendation\ContentRecommendationService $contentRecommendationService */
    $contentRecommendationService = \Drupal::service('content_recommendation.recommendation');

    $recommended_content_categories = $contentRecommendationService->getContentRecommendation($entity, $query);

    /** @var \Drupal\hn\HnResponseService $hn */
    $hn = \Drupal::service('hn.response');

    $response = [];

    foreach ($recommended_content_categories as $recommended_content_category => $items) {
      $response[$recommended_content_category] = [];

      foreach ($items as $item) {
        /** @var EntityInterface $item */
        $response[$recommended_content_category][] = $item->uuid();
        $hn->addEntity($item, 'teaser');
      }
    }

    return $response;
  }

  public function onPostHandle(HnHandledEntityEvent $event) {

    /** @var \Drupal\Core\Entity\ContentEntityBase $entity */
    $entity = $event->getEntity();

    $handled_entity = $event->getHandledEntity();

    $should_get_content_recommendation = static::entitySupportsContentRecommendation($entity, $event->getViewMode());
    $handled_entity['content_recommendation'] = $should_get_content_recommendation;

    $event->setHandledEntity($handled_entity);

  }

}
