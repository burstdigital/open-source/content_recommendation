<?php

namespace Drupal\content_recommendation_highlights\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Class ConfigForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_recommendation_highlights.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_recommendation_highlights_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('content_recommendation_highlights.settings');

    $highlights = $config->get('highlighted_nodes') ?: [];

    // TODO: make unlimited value field with add/remove buttons. https://www.drupal.org/forum/support/module-development-and-code-questions/2016-04-28/how-to-add-addmoreremove-buttons-for
    $form['highlighted_nodes'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Highlighted nodes'),
      '#default_value' => Node::loadMultiple(array_column($highlights, 'target_id')),
      '#tags' => TRUE,
      '#maxlength' => 255,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the config.
    $this->config('content_recommendation_highlights.settings')
      ->set('highlighted_nodes', $values['highlighted_nodes'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
