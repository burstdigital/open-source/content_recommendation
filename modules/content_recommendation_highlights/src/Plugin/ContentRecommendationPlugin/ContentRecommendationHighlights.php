<?php

namespace Drupal\content_recommendation_highlights\Plugin\ContentRecommendationPlugin;

use Drupal\content_recommendation\Plugin\ContentRecommendationPluginBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;

/**
 * Provides content recommendation for highlighted content
 *
 * @ContentRecommendationPlugin(
 *   id = "highlights",
 *   priority = 10
 * )
 */
class ContentRecommendationHighlights extends ContentRecommendationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function recommend(EntityInterface $entity, $query) {

    $config = \Drupal::config('content_recommendation_highlights.settings');

    $highlights = $config->get('highlighted_nodes') ?: [];
    $nids = array_column($highlights, 'target_id');

    $nodes = [];

    if (!empty($nids)) {
      $nodes = Node::loadMultiple($nids);
    }

    return $nodes;

  }

}
